import CanvasComponent from './CanvasComponent';
import Grid from './Grid';

export default class extends CanvasComponent {
  data: Grid<number>;
  max: number;
  onMouseMove: ?(i1: number, i2: number) => void;
  onMouseOut: ?() => void;
  constructor(dom: HTMLElement, width: number = 305, height: number = 305) {
    super(dom, width, height);
    this.data = new Grid(1, 1);
    this.max = 1;
    this.dom.addEventListener('mousemove', (e: MouseEvent) => this.handleMouseMove(e));
    this.dom.addEventListener('mouseout', (e: MouseEvent) => this.handleMouseOut());
  }
  handleMouseMove(e: MouseEvent) {
    const x = e.clientX - this.dom.offsetLeft;
    const y = e.clientY - this.dom.offsetTop;
    const [i1, i2] = this.unmapDatum(x, y);
    if (!this.onMouseMove) return;
    this.onMouseMove(i1, i2);
  }
  handleMouseOut() {
    if (!this.onMouseOut) return;
    this.onMouseOut();
  }
  draw() {
    this.clear();
    this.data.forEach((i1, i2) => {
      const value = this.data.getAt(i1, i2);
      const color = Math.round((1 - (value/this.max)) * 255);
      this.ctx.fillStyle = `rgba(${color},${color},${color},1)`;
      this.ctx.fillRect(...this.mapDatum(i1, i2));
    });
  }
  mapDatum(i1: number, i2: number) {
    const step1 = this.dom.width/this.data.i1max;
    const step2 = this.dom.height/this.data.i2max;
    return [i1*step1, i2*step2, step1, step2].map(Math.ceil);
  }
  unmapDatum(x: number, y: number) {
    const i1 = Math.floor(x/this.dom.width*this.data.i1max);
    const i2 = Math.floor(y/this.dom.height*this.data.i2max);
    return [i1, i2];
  }
}
