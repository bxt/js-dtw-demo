import Sequences from './Sequences';
import type DTW from './DTW';

const color = alpha => `rgba(255,153,0,${alpha})`;

export default class extends Sequences {
  data: ?[number, number];
  constructor(dom: HTMLElement, dtw: DTW, space?: number) {
    super(dom, dtw, space);
    [this.dtw, this.dtw.distanceMatrix].forEach(m => {
      m.onMouseMove = (i1, i2) => {
        this.data = [i1, i2];
        this.notifyChange();
      };
      m.onMouseOut = () => {
        this.data = null;
        this.notifyChange();
      };
    });
  }
  draw() {
    this.clear();
    if (this.data) {
      const [i1, i2] = this.data;
      this.ctx.strokeStyle = color(0.8);
      const i1datum = this.dtw.getSequence1().data[i1];
      const i2datum = this.dtw.getSequence2().data[i2];
      this.drawPath([
        this.dtw.getSequence1().mapDatum(i1, i1datum),
        this.applyOffset(this.dtw.getSequence2().mapDatum(i2, i2datum)),
      ]);
      this.ctx.strokeStyle = color(0.6);
      this.drawPath([
        this.dtw.getSequence1().mapDatum(0, i1datum),
        this.dtw.getSequence1().mapDatum(this.dtw.getSequence1().data.length, i1datum),
      ]);
      this.drawPath([
        this.applyOffset(this.dtw.getSequence2().mapDatum(0, i2datum)),
        this.applyOffset(this.dtw.getSequence2().mapDatum(this.dtw.getSequence2().data.length, i2datum)),
      ]);
      this.ctx.strokeStyle = color(0.2);
      this.drawPath([
        this.applyOffset(this.dtw.getSequence1().mapDatum(0, i1datum)),
        this.applyOffset(this.dtw.getSequence1().mapDatum(this.dtw.getSequence1().data.length, i1datum)),
      ]);
      this.drawPath([
        this.dtw.getSequence2().mapDatum(0, i2datum),
        this.dtw.getSequence2().mapDatum(this.dtw.getSequence2().data.length, i2datum),
      ]);
    }
  }
}
