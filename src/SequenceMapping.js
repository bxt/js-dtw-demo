import Sequences from './Sequences';

export default class extends Sequences {
  draw() {
    this.clear();
    this.ctx.strokeStyle = 'rgba(0,0,255,0.1)';
    this.dtw.getPath().forEach(([i1, i2]) => {
      this.drawPath([
        this.dtw.getSequence1().mapDatum(i1, this.dtw.getSequence1().data[i1]),
        this.applyOffset(this.dtw.getSequence2().mapDatum(i2, this.dtw.getSequence2().data[i2])),
      ]);
    });
  }
}
