export default class Component<T> {
  dom: T;
  onChange: Array<() => void>;
  constructor(dom: T) {
    this.dom = dom;
    this.onChange = [this.calculate, this.draw];
  }
  calculate() {}
  draw() {}
  notifyChange() {
    this.onChange.forEach(callback => callback.call(this));
  }
  listenChanges(of: Component<*>) {
    of.onChange.push(e => this.notifyChange());
  }
}
