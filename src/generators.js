function runLengthDecode(rle: [number, number][]): number[] {
  return [].concat(...rle.map(([n, v]) => new Array(n).fill(v)));
}

function normalize(sequence: number[]): number[] {
  const max = Math.max(...sequence);
  const min = Math.min(...sequence);
  const range = max - min;
  return sequence.map(y => (y - min)/range);
}

export const custom1 = () => [43, 44, 44, 42, 39, 38, 34, 33, 33, 32, 31, 30, 30, 28, 28, 26, 24, 24, 23, 23, 24, 27, 32, 38, 44, 49, 52, 55, 57, 61, 68, 71, 74, 73, 73, 76, 76, 77, 77, 76, 75, 74, 72, 71, 69, 28, 58, 66, 64, 62, 60, 58, 57, 55, 53, 51, 49, 39, 33, 34].map((x) => x/100);
export const custom2 = () => [50, 52, 50, 49, 46, 43, 39, 37, 34, 32, 30, 67, 25, 23, 23, 22, 22, 22, 22, 23, 26, 29, 33, 34, 37, 40, 41, 41, 42, 43, 44, 44, 45, 47, 50, 55, 58, 63, 64, 66, 66, 69, 72, 74, 75, 73, 67, 64, 58, 54, 49, 43, 37, 34, 27, 25, 22, 20, 20, 19].map((x) => x/100);

export function digital(shift?: boolean): number[] {
  if (shift) {
    return runLengthDecode([[20, 0.5], [30, 0.8], [5, 0.2], [5, 0.5]]);
  } else {
    return runLengthDecode([[15, 0.5], [15, 0.8], [15, 0.2], [15, 0.5]]);
  }
}

export function flat(samples: number, level?: number = 0.5): number[] {
  return runLengthDecode([[samples, level]]);
}

type sineOptions = {
     shift?: boolean,
     phase?: number,
     frequency?: number,
     scaling?: number,
}

export function sine(samples: number, options?: sineOptions = {}): number[] {
  const {
    shift = false,
    phase = 0,
    frequency = 1/samples,
    scaling = 0.8,
  } = options;
  const mapToRange = y => y * scaling * 0.5 + 0.5;
  const sequence = new Array(samples);
  for (let i = 0; i < samples; i++) {
    const x = 2*Math.PI * frequency * i - phase;
    const offset = shift ? Math.sin(x*0.5) : 0;
    sequence[i] = mapToRange(Math.sin(x - offset));
  }
  return sequence;
}

export function random(samples: number): number[] {
  const sequence = new Array(samples);
  let prev = 0;
  for (let i = 0; i < samples; i++) {
    sequence[i] = prev;
    prev = prev + Math.random() - 0.5;
  }
  return normalize(sequence);
}
