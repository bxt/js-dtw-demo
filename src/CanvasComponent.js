import Component from './Component';

export default class extends Component<HTMLCanvasElement> {
  ctx: CanvasRenderingContext2D;
  constructor(dom: HTMLElement, width: number, height: number) {
    if (!(dom instanceof HTMLCanvasElement)) throw new Error('Must supply a canvas');
    super((dom));
    this.dom.width = width;
    this.dom.height = height;
    const ctx = this.dom.getContext('2d');
    if (!ctx) throw new Error('Canvas not supported!');
    this.ctx = ctx;
  }
  clear() {
    this.ctx.clearRect(0, 0, this.dom.width, this.dom.height);
  }
  drawPath(pathCoords: Array<number[]>) {
    this.ctx.beginPath();
    pathCoords.forEach(([x, y], i) => {
      if (i === 0) {
        this.ctx.moveTo(x, y);
      } else {
        this.ctx.lineTo(x, y);
      }
    });
    this.ctx.stroke();
  }
}
