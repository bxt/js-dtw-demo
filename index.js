import Sequence from './src/Sequence';
import DistanceMatrix from './src/DistanceMatrix';
import DTW from './src/DTW';
import AdjustedSequence from './src/AdjustedSequence';
import SequenceMapping from './src/SequenceMapping';
import DtwString from './src/DtwString';
import SequenceReference from './src/SequenceReference';
import SampleLoader from './src/SampleLoader';

function g(id) {
  const element = document.getElementById(id);
  if (element == null) throw `Required element ${id} not on page!`;
  return element;
}

const sequence1 = new Sequence(g('sequence1'));
const sequence2 = new Sequence(g('sequence2'));

const distanceMatrix = new DistanceMatrix(g('distanceMatrix'), sequence1, sequence2);
const dtw = new DTW(g('dtw'), distanceMatrix);

const adjustedSequence = new AdjustedSequence(g('adjustedSequence'), dtw);
const sequenceMapping = new SequenceMapping(g('sequenceMapping'), dtw);

const dtwString = new DtwString(g('dtwString'), dtw);

const sequenceReference = new SequenceReference(g('sequenceReference'), dtw);

const sampleLoader = new SampleLoader(g('sampleLoader'), sequence1, sequence2);
sampleLoader.loadData(0);
