import Matrix from './Matrix';
import Grid from './Grid';
import type Sequence from './Sequence';

export default class extends Matrix {
  sequence1: Sequence;
  sequence2: Sequence;
  constructor(dom: HTMLElement, sequence1: Sequence, sequence2: Sequence) {
    super(dom);
    this.sequence1 = sequence1;
    this.sequence2 = sequence2;
    this.listenChanges(sequence1);
    this.listenChanges(sequence2);
  }
  calculate() {
    this.data = new Grid(this.sequence1.data.length, this.sequence2.data.length);
    this.data.forEach((i1, i2) => {
      const value = Math.abs(this.sequence1.data[i1] - this.sequence2.data[i2]);
      this.data.setAt(i1, i2, value);
    });
  }
}
