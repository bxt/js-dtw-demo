import CanvasComponent from './CanvasComponent';
import type DTW from './DTW';

export default class extends CanvasComponent {
  dtw: DTW;
  constructor(dom: HTMLElement, dtw: DTW) {
    const { width, height } = dtw.getSequence2().dom;
    super(dom, width, height);
    this.dtw = dtw;
    this.listenChanges(dtw);
  }
  draw() {
    this.clear();
    this.drawPath(this.dtw.getPath().map(([i1, i2]) => {
      return this.dtw.getSequence2().mapDatum(i2, this.dtw.getSequence1().data[i1]);
    }));
  }
}
