import {
  custom1,
  custom2,
  flat,
  digital,
  sine,
  random,
} from './generators';

type sample = {
  label: string,
  generate: () => [number[], number[]],
}

const samples: sample[] = [
  {
    label: 'custom',
    generate: () => [custom1(), custom2()],
  },
  {
    label: 'flat',
    generate: () => [flat(30), flat(30)],
  },
  {
    label: 'flat - different samples',
    generate: () => [flat(30), flat(10)],
  },
  {
    label: 'digital',
    generate: () => [digital(), digital(true)],
  },
  {
    label: 'sine',
    generate: () => [sine(60, { shift: true }), sine(60)],
  },
  {
    label: 'sine HQ',
    generate: () => [sine(300, { shift: true }), sine(300)],
  },
  {
    label: 'sine shifted',
    generate: () => [sine(60, { phase: Math.PI*0.5 }), sine(60)],
  },
  {
    label: 'badly normalized',
    generate: () => [flat(30, 0.2), flat(30, 0.8)],
  },
  {
    label: 'random walks',
    generate: () => [random(30), random(30)],
  },
];

export default samples;
