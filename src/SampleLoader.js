import Component from './Component';
import samples from './samples';
import type Sequence from './Sequence';

export default class extends Component<HTMLElement> {
  sequences: Sequence[];
  data: typeof samples;
  constructor(dom: HTMLElement, sequence1: Sequence, sequence2: Sequence) {
    super(dom);
    this.sequences = [sequence1, sequence2];
    this.data = samples;
    this.notifyChange();
  }
  draw() {
    this.dom.innerHTML = this.data.map(({ label }) => {
      return `<a href="javascript:;">${label}</a>`;
    }).join(', ');
    this.data.forEach((_, i) => {
      this.dom.children[i].addEventListener('click', () => this.loadData(i));
    });
  }
  loadData(index: number) {
    const newData = this.data[index].generate();
    this.sequences.forEach((sequence, i) => {
      sequence.setData(newData[i]);
    });
  }
}
