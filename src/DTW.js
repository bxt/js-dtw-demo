import Matrix from './Matrix';
import Grid from './Grid';
import type DistanceMatrix from './DistanceMatrix';

export default class extends Matrix {
  distanceMatrix: DistanceMatrix;
  pathData: Grid<[number, number]>;
  constructor(dom: HTMLElement, distanceMatrix: DistanceMatrix) {
    super(dom);
    this.distanceMatrix = distanceMatrix;
    this.listenChanges(distanceMatrix);
  }
  calculate() {
    const dimensions = [this.distanceMatrix.data.i1max, this.distanceMatrix.data.i2max];
    this.data = new Grid(...dimensions);
    this.pathData = new Grid(...dimensions);
    this.max = Number.NEGATIVE_INFINITY;
    this.data.forEach((i1, i2) => {
      const distanceCost = this.distanceMatrix.data.getAt(i1, i2);
      const coords = [[i1 - 1, i2 - 1], [i1 - 1, i2], [i1, i2 - 1]];
      const minCost = Math.min(...coords.map(c => this.getDataAt(...c)));
      const minCoord = coords.find(c => this.getDataAt(...c) === minCost);
      if (!minCoord) throw new Error('Unexpected state');
      const value = distanceCost + minCost;
      this.max = Math.max(this.max, value);
      this.data.setAt(i1, i2, value);
      this.pathData.setAt(i1, i2, minCoord);
    });
  }
  getDataAt(i1: number, i2: number) {
    if (i1 < 0 && i2 < 0) {
      return 0;
    } else if (i1 < 0 || i2 < 0) {
      return Number.POSITIVE_INFINITY;
    } else {
      return this.data.getAt(i1, i2);
    }
  }
  getPath() {
    const path = [];
    let coords = [this.data.i1max - 1, this.data.i2max - 1];
    do {
      path.push(coords);
      coords = this.pathData.getAt(...coords);
    } while (coords);
    return path;
  }
  getDistance() {
    return this.getDataAt(this.data.i1max - 1, this.data.i2max - 1);
  }
  draw() {
    super.draw();
    this.ctx.strokeStyle = 'rgba(0,0,255,0.5)';
    this.drawPath(this.getPath().map(is => {
      const [x, y, xWidth, yWidth] = this.mapDatum(...is);
      return [x + xWidth/2, y + yWidth/2];
    }));
  }
  getSequence1() {
    return this.distanceMatrix.sequence1;
  }
  getSequence2() {
    return this.distanceMatrix.sequence2;
  }
}
