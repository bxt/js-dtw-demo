import CanvasComponent from './CanvasComponent';

export default class extends CanvasComponent {
  editing: boolean;
  data: number[];
  datumWidth: number;
  constructor(dom: HTMLElement, width: number = 300, height: number = 100) {
    super(dom, width, height);
    this.editing = false;
    this.data = [1];
    this.dom.addEventListener('mousemove', (e: MouseEvent) => this.onMouseEdit(e));
    this.dom.addEventListener('mousedown', (e: MouseEvent) => { this.editing = true; this.onMouseEdit(e); });
    document.addEventListener('mouseup', () => { this.editing = false; });
  }
  onMouseEdit(e: MouseEvent) {
    if (this.editing) {
      const x = e.clientX - this.dom.offsetLeft;
      const y = e.clientY - this.dom.offsetTop;
      const [i, datum] = this.unmapDatum(x, y);
      this.data[i] = datum;
      this.notifyChange();
    }
  }
  setData(data: number[]) {
    this.data = data;
    this.notifyChange();
  }
  calculate() {
    this.datumWidth = this.dom.width / (this.data.length-1);
  }
  draw() {
    this.clear();
    this.drawPath(this.data.map((datum, i) => this.mapDatum(i, datum)));
  }
  mapDatum(i: number, datum: number) {
    return [i*this.datumWidth, datum*this.dom.height];
  }
  unmapDatum(x: number, y: number) {
    return [Math.round(x/this.datumWidth), y/this.dom.height];
  }
}
