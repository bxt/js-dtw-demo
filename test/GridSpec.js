import { expect } from 'chai';
import Grid from '../src/Grid';

describe('Grid', function () {
  describe('#constructor(i1max, i2max)', function () {
    it('should set i1max and i2max', function () {
      const grid = new Grid(2, 3);
      expect(grid.i1max).to.equal(2);
      expect(grid.i2max).to.equal(3);
    });
  });
  describe('#setAt(i1, i2, value) ', function () {
    it('should set a value for #getAt()', function () {
      const grid = new Grid(2, 3);
      expect(grid.getAt(1, 1)).to.be.undefined;
      grid.setAt(1, 1, 'test');
      expect(grid.getAt(1, 1)).to.equal('test');
    });
    it('should not set any value for #getAt()', function () {
      const grid = new Grid(2, 3);
      expect(grid.getAt(1, 2)).to.be.undefined;
      grid.setAt(1, 1, 'test');
      expect(grid.getAt(1, 2)).to.be.undefined;
    });
  });
  describe('#forEach(callback)', function () {
    it('should call the callback with coords for each entry', function () {
      const grid = new Grid(2, 3);
      grid.setAt(1, 1, 'test1');
      grid.setAt(1, 2, 'test2');
      const calls = [];
      grid.forEach((...args) => calls.push(args));
      expect(calls).to.eql([
        [0, 0],
        [0, 1],
        [0, 2],
        [1, 0],
        [1, 1],
        [1, 2],
      ]);
    });
  });
});
