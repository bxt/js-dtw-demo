import Component from './Component';
import type DTW from './DTW';

export default class extends Component<HTMLElement> {
  dtw: DTW;
  constructor(dom: HTMLElement, dtw: DTW) {
    super(dom);
    this.dtw = dtw;
    this.listenChanges(dtw);
  }
  draw() {
    this.dom.innerHTML = `(dist=${this.formattedDistance()})`;
  }
  formattedDistance() {
    const digits = 5;
    const factor = Math.pow(10, digits);
    const distance = this.dtw.getDistance();
    return Math.round(distance*factor)/factor;
  }
}
