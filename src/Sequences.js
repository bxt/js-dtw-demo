import CanvasComponent from './CanvasComponent';
import type DTW from './DTW';

export default class extends CanvasComponent {
  offset: number;
  dtw: DTW;
  constructor(dom: HTMLElement, dtw: DTW, space?: number = 46) {
    const offset = dtw.getSequence1().dom.height + space;
    const width = Math.max(dtw.getSequence1().dom.width, dtw.getSequence2().dom.width);
    super(dom, width, offset + dtw.getSequence2().dom.height);
    this.offset = offset;
    this.dtw = dtw;
    this.listenChanges(dtw);
  }
  applyOffset(coords: [number, number]) {
    return coords.map((x, i) => i === 1 ? x + this.offset : x);
  }
}
