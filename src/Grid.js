export default class <T> {
  i1max: number;
  i2max: number;
  array: T[];
  constructor(i1max: number, i2max: number) {
    this.i1max = i1max;
    this.i2max = i2max;
    this.array = new Array(i1max*i2max);
  }
  getAt(i1: number, i2: number): T {
    return this.array[i1*this.i2max + i2];
  }
  setAt(i1: number, i2: number, value: T): void {
    this.array[i1*this.i2max + i2] = value;
  }
  forEach(callback: (i1: number, i2: number) => void) {
    for (let i1 = 0; i1 < this.i1max; i1++) {
      for (let i2 = 0; i2 < this.i2max; i2++) {
        callback(i1, i2);
      }
    }
  }
}
