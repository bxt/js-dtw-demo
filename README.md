js-dtw-demo
===========

An interactive demo of the dynamic time warping (DTW) algorithm.

Run the demo here: https://bxt.gitlab.io/js-dtw-demo/

Developing
---------

To **install** all the required packages and **compile** `index.min.js` do:

    npm install

Then open `index.html` in your browser.

If you want to **watch** for changes to recompile do:

    npm start

To run the **tests** ([assertions info](http://chaijs.com/api/bdd/)) use:

    npm test
    # or
    npm test -- -w

To **lint** the JS run:

    $(npm bin)/eslint src/**

Use `--fix` to fix some stuff automatically.

To run **flow** do:

    npm run-script flow

This will type-check the code.
